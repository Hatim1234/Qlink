//
//  mealsCell.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2020-03-01.
//  Copyright © 2020 Hatim Belhadjhamida. All rights reserved.
//

import UIKit

class mealsCell: UITableViewCell {
    @IBOutlet weak var mealName: UILabel!
    
    @IBOutlet weak var mealList: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var data: [String:String]? {
       didSet {
        guard let data = data else { return }
        
        self.mealList.numberOfLines = 0
        
        for ele in data{
            self.mealName.text = ele.key
            self.mealList.text = ele.value
        }
        
        var Height = self.mealList.optimalHeight
               
       self.mealList.frame = CGRect(x: self.mealList.frame.origin.x, y: self.mealList.frame.origin.y, width: self.mealList.frame.width, height: Height)
        
        }

}
    

}
