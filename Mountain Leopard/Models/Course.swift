//
//  Course.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2019-08-11.
//  Copyright © 2019 Hatim Belhadjhamida. All rights reserved.
//

import Foundation
import UIKit

class Course: student{
    
    
    var courseName:String
    var startDate: String
    var endDate: String
    var backgroundImage: UIImage?
    var days:[String:[String]]
    var listImages: [String] = ["BEAMISH-MUNRO", "GOODES", "MACINTOSH-CORRY",
    "BIOSCI AUD", "GOODWIN", "MCLAUGHLIN", "BOTTER", "HUMPHREY AUD", "MILLER",
    "CHERNOFF AUD", "ISABEL", "MITCHELL", "CONVOCATION HALL", "JACKSN", "NICOL",
    "DUNCAN MCARTHUR", "JEFFERY", "ONTARIO", "DUNNING AUD", "JOHN WATSON",
    "RICHARDSON", "DUPUIS AUD", "KINES & HLTH", "STIRLING", "ELLIS", "KINGSTON",
    "THEOLOGICAL", "ETHERINGTON AUD", "MACDONALD", "WALTER LIGHT"
]
    
    init(courseName:String, Location: String, startDate: String, endDate: String, days: [String:[String]]) {
        var courseNames: Array<String> = courseName.components(separatedBy: " ")
        
        self.courseName = courseNames[0] + " " + courseNames[1]
        
        self.startDate = startDate
        self.endDate = endDate
        self.days = days
    
        
        for ele in listImages{
            
            if Location.contains(ele) {
                self.backgroundImage = UIImage(named: ele) ?? (UIImage(named: "Default") ?? nil)!
               
                break
            }
            
            
        }
        
        if self.backgroundImage == nil{
            
            backgroundImage = (UIImage(named: "Default") ?? nil)!
        }
        
        
        super.init(Location: Location)
        
        
    }
    
    func getDate() -> Date{
        let twendateFormatter = DateFormatter()
        twendateFormatter.dateFormat = "yyyyMMdd'T'HHmmss"
        
        return twendateFormatter.date(from: self.startDate)!
    }
    
    func getDiff() -> TimeInterval{
        let twendateFormatter = DateFormatter()
        twendateFormatter.dateFormat = "yyyyMMdd'T'HHmmss"
        var diff: Date = twendateFormatter.date(from: self.startDate)!
        
        return diff.timeIntervalSinceNow
    }
    
    
    
    func getCourseName() -> String {
        return self.courseName
    }
    
    func getLocation() -> String {
        return self.Location
    }
    
    func getStartDate() -> String {
        return self.startDate
    }
    
    func getEndDate() -> String {
        return self.endDate
    }
    
    func getDays() -> [String:String]{
        let twendateFormatter = DateFormatter()
        let tweldateFormatter = DateFormatter()
        twendateFormatter.dateFormat = "HHmmss"
        tweldateFormatter.dateFormat = "h:mm a"
        
        
        var dayDict: [String:String] = [:]
        
        for ele in self.days{
            
            let start = ele.value[0].components(separatedBy: "T")[1]
            
            let end = ele.value[1].components(separatedBy: "T")[1]
            
            print(start)
            print(end)
            
            let startTime = tweldateFormatter.string( from: twendateFormatter.date(from: start)!)
            
            let endTime = tweldateFormatter.string( from: twendateFormatter.date(from:end)!)
            
            dayDict[ele.key] = startTime + "-" + endTime
            
        }
        
       
        
     
    
        return dayDict
    }
    
    
    
    
    
    
    
}
