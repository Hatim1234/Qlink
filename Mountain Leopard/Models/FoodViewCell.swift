//
//  FoodViewCell.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2020-03-01.
//  Copyright © 2020 Hatim Belhadjhamida. All rights reserved.
//

import UIKit
import SwiftyJSON

class FoodViewCell: UICollectionViewCell {
    
    @IBOutlet weak var foodTable: UIDynamicTableView!
    
    @IBOutlet weak var TypeMeal: UILabel!
    
    var datas: [[String:String]] = []
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    var data: JSON? {
    didSet {
        guard let data = data else { return }
        var datas: [[String:String]] = []
         for ele in data{
            var dat: [String:String] = [:]
            let mealName = ele.0.description
            let meals = ele.1.arrayObject as? [String]
            dat[mealName] = ""
            for ele in meals!{
                dat[mealName]! += (ele + "\n")
            }
            
            print(datas)
            
            datas.append(dat)
            
        }
        
        self.datas = datas
        
        }
        
    }
    
    override func awakeFromNib() {
           super.awakeFromNib()
        foodTable.reloadData()
        
        
        self.TypeMeal.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
            foodTable.rowHeight = UITableView.automaticDimension
            foodTable.estimatedRowHeight = 3000
        
        foodTable.delegate = self
        foodTable.dataSource = self
        self.backgroundImage.roundCorners(corners: .allCorners, radius: 16)
        
        self.foodTable.roundCorners(corners: .allCorners, radius: 16)
        
       
           }
    
   
    
//    override func preferredLayoutAttributesFitting(_ layoutAttributes: UICollectionViewLayoutAttributes) -> UICollectionViewLayoutAttributes {
//           setNeedsLayout()
//           layoutIfNeeded()
//            let size = contentView.systemLayoutSizeFitting(layoutAttributes.size)
//
//              var frame = layoutAttributes.frame
//              frame.size.height = ceil(size.height)
//
//              layoutAttributes.frame = frame
//
//              return layoutAttributes
//       }
    
    
    
    
    
    
    
    }


extension FoodViewCell: UITableViewDataSource,
           UITableViewDelegate
    
    
    {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return self.datas.count
        }
    
    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "foodCell", for: indexPath) as! mealsCell
        cell.data = self.datas[indexPath.item]
        
        return cell
            
        }
        
        
    }
    
    
    


