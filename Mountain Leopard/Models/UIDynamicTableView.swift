//
//  UIDynamicTableView.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2020-03-03.
//  Copyright © 2020 Hatim Belhadjhamida. All rights reserved.
//

import UIKit

class UIDynamicTableView: UITableView {

   override var intrinsicContentSize: CGSize {
            self.layoutIfNeeded()
    return CGSize(width: UIView.noIntrinsicMetric, height: self.contentSize.height)
        }

        override func reloadData() {
            super.reloadData()
            self.invalidateIntrinsicContentSize()
        }
    }
