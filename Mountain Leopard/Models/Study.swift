//
//  Study.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2020-01-03.
//  Copyright © 2020 Hatim Belhadjhamida. All rights reserved.
//

import Foundation

class Study: student{
    
    
    var closeOpen: Bool
    var hours: String
    
    init(Location: String, closeOpen: Bool, hours: String) {
        self.closeOpen = closeOpen
        self.hours = hours
        super.init(Location: Location)
    }
    
    
 
}
