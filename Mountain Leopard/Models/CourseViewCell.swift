//
//  CourseViewCell.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2019-12-11.
//  Copyright © 2019 Hatim Belhadjhamida. All rights reserved.
//

import UIKit

class CourseViewCell: UICollectionViewCell {
    
    
    required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
    }

    @IBOutlet weak var imageBack: UIImageView!
    
    @IBOutlet weak var courseName: UILabel!
    
    @IBOutlet weak var time: UILabel!
    
    @IBOutlet weak var location: UILabel!
    
     var data: Course? {
            didSet {
                guard let data = data else { return }
                
                

               
                self.courseName.text = data.courseName
                
                let twendateFormatter = DateFormatter()
                let tweldateFormatter = DateFormatter()
                twendateFormatter.dateFormat = "HHmmss"
                tweldateFormatter.dateFormat = "h:mm a"
                
                let start = data.startDate.components(separatedBy: "T")[1]
                let end = data.endDate.components(separatedBy: "T")[1]
                
                
                
                let startTime = tweldateFormatter.string( from: twendateFormatter.date(from: start)!)
                
                let endTime = tweldateFormatter.string( from: twendateFormatter.date(from:end)!)
                
                self.time.text = startTime + "-" + endTime
                
                self.location.text = data.getLocation()
                
                
                
                
                
                
            }
            
           
        }
        
        
        
        
        override func awakeFromNib() {
            super.awakeFromNib()
            
            
            
            
            contentView.addSubview(courseName)
            contentView.addSubview(time)
            contentView.addSubview(location)
            
            
            self.imageBack.roundCorners(corners: [.topLeft, .bottomLeft], radius: 10.0)
            
           
        }
    
    
        
       
        
        
    

}
