//
//  loadDays.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2019-09-08.
//  Copyright © 2019 Hatim Belhadjhamida. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

func closestDate(courses:[Course]) -> Course {
    
    let calendar = Calendar.current
    let dates = calendar.dateComponents([.day, .month, .year, .hour, .minute, .calendar], from: Date()).date
    
    var coursesMut = courses
   
    coursesMut.sorted(by: { $0.getDiff() < $1.getDiff() })
        
    
    return coursesMut[0]
    
}

func organizeCourses(courses:[Course]) -> [Course]{
    
    var coursesMut = courses
    
    return coursesMut.sorted(by: { $0.getDate() < $1.getDate() })
}

func loadDays() -> [Date]{
    
    let formatter = DateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    formatter.timeZone = TimeZone(abbreviation: "EST")
    let estDate = formatter.string(from: Date())
    
    var days = DateComponents()
    
    days.month = -6
    
    var lowerRange = Calendar.current.date(byAdding: days, to: Date())!
    days.month = 6
    
    var upperRange = Calendar.current.date(byAdding: days, to: Date())!
    
    lowerRange = lowerRange.next(.sunday)
    upperRange = upperRange.next(.saturday)
    
    
    var test: [Date] = datesRange(from: lowerRange, to: upperRange)
    
    return test
}

func datesRange(from: Date, to: Date) -> [Date] {
    // in case of the "from" date is more than "to" date,
    // it should returns an empty array:
    if from > to { return [Date]() }
    
    var tempDate = from
    var array = [tempDate]
    
    while tempDate < to {
        tempDate = Calendar.current.date(byAdding: .day, value: 1, to: tempDate)!
        array.append(tempDate)
    }
    
    return array
}

