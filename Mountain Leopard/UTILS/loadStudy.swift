//
//  loadStudy.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2020-01-03.
//  Copyright © 2020 Hatim Belhadjhamida. All rights reserved.
//

import Foundation
import UIKit

var DATASTUDY: [String:[String:[Int]]] = ["Stauffer Library":["Sunday":[8,0,2,0], "Monday":[8,0,2,0], "Tuesday":[8,0,2,0], "Wednesday":[8,0,2,0], "Thursday":[8,0,2,0], "Friday":[8,0,2,0], "Saturday":[8,0,2,0]], "Douglas Library":["Sunday":[10,0,23,0], "Monday":[8,30,23,0], "Tuesday":[8,30,23,0], "Wednesday":[8,30,23,0], "Thursday":[8,30,23,0], "Friday":[8,30,23,0], "Saturday":[10,0,23,0]], "Lederman Law Library":  ["Sunday":[10,0,19,0], "Monday":[8,30,23,0], "Tuesday":[8,30,23,0], "Wednesday":[8,30,23,0], "Thursday":[8,30,23,0], "Friday":[8,30,16,30], "Saturday":[10,0,16,30]], "Bracken Health Sciences Library":["Sunday":[12,0,19,0], "Monday":[8,30,23,0], "Tuesday":[8,30,20,0], "Wednesday":[8,30,20,0], "Thursday":[8,30,20,0], "Friday":[8,30,20,0], "Saturday":[12,0,19,0]], "Education Library":["Sunday":[10,0,16,30], "Monday":[8,0,21,0], "Tuesday":[8,0,21,0], "Wednesday":[8,0,21,0], "Thursday":[8,0,21,0], "Friday":[8,0,21,0], "Saturday":[10,0,16,30]], "University Archives":["Sunday":[0,0,0,0], "Monday":[8,30,16,30], "Tuesday":[8,30,16,30], "Wednesday":[8,30,16,30], "Thursday":[8,30,16,30], "Friday":[8,30,16,30], "Saturday":[0,0,0,0]]]

func loadData() -> [String:[String:[Date]]] {
    var data = [String:[String:[Date]]]()
    for (name, times) in DATASTUDY{
        for (dayOf, time) in times{
            
           
            var calendar = Calendar.current
            var startDateComponents = calendar.dateComponents([.year, .month, .day], from: Date())
            startDateComponents.hour = time[0]
            startDateComponents.minute = time[1]
            
            var endDateComponents = calendar.dateComponents([.year, .month, .day], from: Date())
            
            if(time[2] <  time[0]){
                endDateComponents.day!+=1
        }
            
            endDateComponents.hour = time[2]
            endDateComponents.minute = time[3]
            
            let startDate = calendar.date(from: startDateComponents)
            let endDate = calendar.date(from: endDateComponents)
            
            
            
            if data[name] == nil{
                
                data.updateValue([dayOf: [startDate!]], forKey: name)
                data[name]?[dayOf]?.append(endDate!)
            }
            else{
                
                data[name]!.updateValue([startDate!], forKey: dayOf)
                data[name]![dayOf]!.append(endDate!)
            }
        }
        
        
        
    }
    return data
    
    
}

func loadStudy() -> [Study] {
    
    var studyData: [Study] = []
    var data: [String:[String:[Date]]] = loadData()
    
    let date = Date()
    
    let customDateFormatter = DateFormatter()
    
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "EEEE"
    let weekday = dateFormatter.string(from: date)
    
    let calendar = Calendar.current
    let dates = calendar.dateComponents([.day, .month, .year, .hour, .minute, .calendar], from: Date()).date
    print(dates)
    
    
    
    
    for (name, weekdays) in data{
        var start = weekdays[weekday]![0]
        var close = weekdays[weekday]![1]
        if (start <= dates! && close >= dates!){
            
            let tweldateFormatter = DateFormatter()
            tweldateFormatter.dateFormat = "h:mm a"
            
            let startTime = tweldateFormatter.string(from: start)
                       
            let endTime = tweldateFormatter.string(from: close)
            
            var strHours = startTime + "-" + endTime
            
            studyData.append(Study(Location: name, closeOpen: true, hours: strHours))
        
                }
        else{
            
            let tweldateFormatter = DateFormatter()
            tweldateFormatter.dateFormat = "h:mm a"
            
            let startTime = tweldateFormatter.string(from: start)
                       
            let endTime = tweldateFormatter.string(from: close)
            
            var strHours = startTime + "-" + endTime
            
            studyData.append(Study(Location: name, closeOpen: false, hours: strHours))
            
            }
        }
    
    return studyData
    }
    
    
   
    
    

