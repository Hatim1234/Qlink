//
//  DashboardViewController.swift
//  
//
//  Created by Hatim Belhadjhamida on 2019-07-13.
//

import UIKit

class DashboardViewController: UIPageViewController, UIPageViewControllerDataSource,
    UIPageViewControllerDelegate

{
    
    
    lazy var subViewControllers:[UIViewController] = {
        return [
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Dashboard") as! ViewControllerDashboard,
//            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Schedule") as! ViewControllerSchedule,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Maps") as! ViewControllerMaps,
            UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Food") as! ViewControllerFood
        ]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self
        // Do any additional setup after loading the view.
        setViewControllers([subViewControllers[0]], direction: .forward, animated: true, completion: nil)
    }
    required init?(coder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
    //MARK - UIPageViewControllerDataSource
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return subViewControllers.count
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewControllers.firstIndex(of: viewController) ?? 0
        if(currentIndex <= 0) {
            return nil
        }
        return subViewControllers[currentIndex-1]
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex:Int = subViewControllers.firstIndex(of: viewController) ?? 0
        if(currentIndex >= subViewControllers.count-1) {
            return nil
        }
        return subViewControllers[currentIndex+1]
    }
}
