//
//  icsParser.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2019-08-11.
//  Copyright © 2019 Hatim Belhadjhamida. All rights reserved.
//

import Foundation
import UIKit

class icsParser: NSObject{
    
    var arrayCourses: Array<Course>
    var icsFile: String
   
    
    
    init(icsFile: String) {
        
        self.icsFile = icsFile
        self.arrayCourses = []
}
    
    
    func getCourses() -> Void {
        var icsContent: Array<String> = self.icsFile.components(separatedBy: "\n")
        var flag: Int = 0
        var courseName: String = ""
        var Location: String = ""
        var startDate: String = ""
        var endDate: String = ""
        var rrule: [String:String] = [:]
        
        for element in icsContent{
            if element == "BEGIN:VEVENT\r"{
                flag = 1
            }
            if element == "END:VEVENT\r"{
                arrayCourses.append(Course(courseName: courseName, Location: Location, startDate: startDate, endDate: endDate, rrule: rrule))
                flag = 0
                

        }
            if flag == 1 {
                let marker = element.firstIndex(of: ":")!
                let checker = String(element[...marker])
                
                
                switch checker {
                case "SUMMARY;LANGUAGE=en-ca:":
                    let marker = element.index(after: element.firstIndex(of: ":")!)
                    courseName = String(element[marker...].filter { !"\n\t\r".contains($0) })
                case "LOCATION:":
                    let marker = element.index(after: element.firstIndex(of: ":")!)
                    Location = String(element[marker...].filter { !"\n\t\r".contains($0) })
                case "DTSTART;TZID=Eastern Standard Time:":
                    let marker = element.index(after: element.firstIndex(of: ":")!)
                    startDate = String(element[marker...].filter { !"\n\t\r".contains($0) })
                case "DTEND;TZID=Eastern Standard Time:":
                    let marker = element.index(after: element.firstIndex(of: ":")!)
                    endDate = String(element[marker...].filter { !"\n\t\r".contains($0) })
                case "RRULE:":
                    
                    let marker = element.index(after: element.firstIndex(of: ":")!)
                    var spliter: String = String(element[marker...].filter { !"\n\t\r".contains($0) })
                    var rrules: Array<String> = spliter.components(separatedBy: ";")
                    for rule in rrules{
                        var keyValues: Array<String> = rule.components(separatedBy: "=")
                        rrule[keyValues[0]] = keyValues[1]
                    }
               default:
                    let a = 1
                }
                
                
            }
            
    }
        
       
        
}
    
    

    
    func getCourse(dateString: String) ->  Array<Course>{
        var coursesDay: Array<Course> = []
        
        for course in arrayCourses{
            
           
            
            let calendar = Calendar.current
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyMMdd'T'HHmmss"
            dateFormatter.timeZone = TimeZone(secondsFromGMT: TimeZone.current.secondsFromGMT())
            
            
            if let entry = course.getRRULE()["UNTIL"] {
                
            var dateComponent = DateComponents()
                dateComponent.day = -1
            
            var endDate = dateFormatter.date(from: entry)
            
            endDate = Calendar.current.date(byAdding: dateComponent, to: endDate!)
            var selectedDate = dateFormatter.date(from: dateString)
                
            var courseDate = dateFormatter.date(from: course.getStartDate())
            courseDate = Calendar.current.date(byAdding: dateComponent, to: courseDate!)
            
            
            var selectedDateCompare = Calendar.current.date(bySetting: .hour , value: 0, of:selectedDate!)
            var courseDateCompare = Calendar.current.date(bySetting: .hour, value: 0, of:courseDate!)
            var endDateCompare = Calendar.current.date(bySetting: .hour , value: 0, of:endDate!)
                
                
            
                if Calendar.current.date(bySetting: .minute , value: 0, of:selectedDateCompare!)! >= Calendar.current.date(bySetting: .minute, value: 0, of:courseDateCompare!)! && Calendar.current.date(bySetting: .minute, value: 0, of:endDateCompare!)! >= Calendar.current.date(bySetting: .minute , value: 0, of:selectedDateCompare!)!{
                
                
                
                }
            else{
                
                continue
                }
            
            let components1 = calendar.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year], from: courseDate!)
            let components2 = calendar.dateComponents([Calendar.Component.day, Calendar.Component.month, Calendar.Component.year], from: selectedDate!)
            
//            var diffInDays = Calendar.current.dateComponents([.day], from: components1, to: components2).day
//
//
//                if let diffInDayss = diffInDays{
//                    diffInDays = diffInDayss - 1
//                }
                
            
            var diffInDays = Calendar.current.dateComponents([.day], from: components1, to: components2).day!
            
            if diffInDays == 0{
                coursesDay.append(course)
                continue
                    
                }
            let monthDiff = components2.month! - components1.month!
                
                
                
            
            if monthDiff % 2 == 0 && monthDiff != 0 && components2.day! != 1 {
                diffInDays+=1
                }
            if components2.month! == 12 || components2.month! == 4{
                    diffInDays+=1
                }
                
            
                
           
            if let val = course.getRRULE()["INTERVAL"]  {
                if (diffInDays-1)%7 != 0{
                    continue
                }
                
                let difference = (diffInDays-1)/7
                
                if difference%2 != 0{
                    coursesDay.append(course)
                }
                
                else{
                    continue
                }
            }
            else{
                let difference = (diffInDays-1)%7
                
                if difference == 0{
                    coursesDay.append(course)
                    
                }
               
                else{
                    continue
                }
                
                
                
                
                
            }
            
            
            
                }
                
            
            
            
            
        
        }
        
       
        
        return coursesDay
    }
    
    
}
