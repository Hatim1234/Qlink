//
//  ViewControllerFood.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2019-07-13.
//  Copyright © 2019 Hatim Belhadjhamida. All rights reserved.
//

import UIKit
import SwiftyJSON

class ViewControllerFood: UIViewController {
    
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var menuCollectionView: UICollectionView!
    var server = StudentServerAPI()
    
    enum Option {
        case Leonard
        case Ban_Righ
        case Jean_Royce
    }
    
    var data: [JSON] = []
    
    var choice = Option.Leonard
    
    @IBOutlet weak var tamCount: UILabel!
    
    @IBOutlet weak var flexCount: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        self.flowLayout.estimatedItemSize = CGSize(width: flowLayout.itemSize.width, height: menuCollectionView.frame.height*2)
        menuCollectionView.delegate = (self as! UICollectionViewDelegate)
        menuCollectionView.dataSource = (self as! UICollectionViewDataSource)
        
        var username = readStringData(key: "username")
        var password = readStringData(key: "password")
        
        server.GetMealPlan(usernameText: username, passwordText: password){ data in
            
            self.flexCount.text = self.flexCount.text! + data["FLEX"]!
            
            self.tamCount.text = self.tamCount.text! + data["TAMS"]!
            
        }
        
        server.Ban_Menu_Current(completionBlock:)(){ response in
            self.data = response
            self.menuCollectionView.reloadData()
        }
        
    }

}

extension ViewControllerFood: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath) as! FoodViewCell
        cell.data = self.data[indexPath.item]
        
        
        return CGSize(width: self.flowLayout.itemSize.width, height: cell.foodTable.frame.height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return data.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "menuCell", for: indexPath) as! FoodViewCell
        cell.data = self.data[indexPath.item]
        return cell
        
    }
    
    
}



