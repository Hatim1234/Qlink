//
//  ViewControllerSchedule.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2019-07-13.
//  Copyright © 2019 Hatim Belhadjhamida. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewControllerSchedule: UIViewController {
    
    var server = StudentServerAPI()
   
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    var data: [Date] = loadDays()
    
    var dataCourse: [Course] = []
    
    @IBOutlet weak var datePicker: UICollectionView!
    
    @IBOutlet weak var CoursesCollectionView: UICollectionView!
    @IBOutlet weak var DaysCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DaysCollectionView.allowsMultipleSelection = false
        DaysCollectionView.translatesAutoresizingMaskIntoConstraints = false
        DaysCollectionView.register(CustomCells.self, forCellWithReuseIdentifier: "cell")
        
        DaysCollectionView.backgroundColor = .clear
        DaysCollectionView.delegate = self
        DaysCollectionView.dataSource = self
        
        CoursesCollectionView.translatesAutoresizingMaskIntoConstraints = false
       
        flowLayout.sectionInset = UIEdgeInsets(top: 2, left: 0, bottom: 0, right: 0)
        flowLayout.minimumInteritemSpacing = 30
        flowLayout.minimumLineSpacing = 30
        CoursesCollectionView.backgroundColor = UIColor.white
        CoursesCollectionView.delegate = self
        CoursesCollectionView.dataSource = self
        
        CoursesCollectionView.tag = 101
        DaysCollectionView.tag = 102
        
       
        }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let today = Date()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.timeZone = TimeZone(abbreviation: "EST")
        let estToday = formatter.string(from: today)
        
        var counter = 0
        let dataCount = data.count
        for ele in data{
            if estToday == formatter.string(from: ele){
                let mod = counter % 7
                self.DaysCollectionView.selectItem(at: IndexPath(item: counter, section: 0), animated: false, scrollPosition: .centeredHorizontally)
                scrollToIndex(index: (counter - mod - 1) + (7 - mod ))
                
                break
            }
            counter += 1
            
        }
        
        
    }
   
        
    func scrollToIndex(index: Int) {
        let indexPath = IndexPath(item: index, section: 0)
        self.DaysCollectionView.scrollToItem(at: indexPath as IndexPath, at: .right, animated: false)
    }
}
    
    
    
    


extension ViewControllerSchedule: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let witdh = scrollView.frame.width - (scrollView.contentInset.left*2)
        let index = scrollView.contentOffset.x / witdh
        let roundedIndex = round(index)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.DaysCollectionView{
           
        return CGSize(width: collectionView.frame.width/7, height: collectionView.frame.width/3.5)
        }
        else{
            return CGSize(width: flowLayout.itemSize.width,  height: flowLayout.itemSize.height)
        }
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.CoursesCollectionView{
            return dataCourse.count
        }
        else{
            return data.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.DaysCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CustomCells
            cell.data = self.data[indexPath.item]
            
            if cell.isSelected{
                
                let layer = CAShapeLayer()
                layer.path = UIBezierPath(roundedRect: CGRect(x: 7, y: -30, width: 35, height: 100), cornerRadius: 50).cgPath
                layer.fillColor = UIColor.white.cgColor
                cell.layer.insertSublayer(layer, at: 0)
                
            }
            else{
                if cell.layer.sublayers!.count > 1{
                    cell.layer.sublayers?.remove(at: 0)
                }
                
            }
                
                return cell
        }
        else{
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cells", for: indexPath) as! CustomCourseCell
            cell.data = self.dataCourse[indexPath.item]
            
            cell.layer.backgroundColor = UIColor.white.cgColor
            cell.layer.cornerRadius = 12
            
            cell.contentView.layer.cornerRadius = 12
            cell.contentView.layer.borderWidth = 1.0
            
            cell.contentView.layer.borderColor = UIColor.clear.cgColor
            cell.contentView.layer.masksToBounds = true
            
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            cell.layer.shadowRadius = 2.0
            cell.layer.shadowOpacity = 1.0
            cell.layer.masksToBounds = false
            cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
            
            return cell
            
            
            
            
            
            
        }
        
       
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        if collectionView == self.DaysCollectionView{
            var day = self.data[indexPath.item]
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            formatter.timeZone = TimeZone(abbreviation: "EST")
            let estDate = formatter.string(from: day).components(separatedBy: "-")
            
            let layer = CAShapeLayer()
            layer.path = UIBezierPath(roundedRect: CGRect(x: 7, y: -30, width: 35, height: 100), cornerRadius: 50).cgPath
            layer.fillColor = UIColor.white.cgColor
            
             let cell = collectionView.cellForItem(at: indexPath)
            
            if cell!.layer.sublayers!.count == 1{
                cell!.layer.insertSublayer(layer, at: 0)
            }
            
            var icsURL = readStringData(key: "icsUrl")
            
            server.GetTodaysCourses(day: estDate[2], month: estDate[1], year: estDate[0], icsUrl: icsURL){ dataStruct in
                self.dataCourse = dataStruct
                self.CoursesCollectionView.reloadData()
            }
        }
        
        
        
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if collectionView == self.DaysCollectionView{
            let cell = collectionView.cellForItem(at: indexPath)
            if (cell ?? nil) != nil{
                if cell!.layer.sublayers!.count > 1{
                    cell!.layer.sublayers?.remove(at: 0)
                }
            }
        
        
        
        
        }
    }
       
        
    }




class CustomCells: UICollectionViewCell {
    
    var data: Date? {
        didSet {
            guard let data = data else { return }
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            formatter.timeZone = TimeZone(abbreviation: "EST")
            let estDate = formatter.string(from: data).components(separatedBy: "-")[2]
            
            title.text = estDate
            
        }
    }
    
    
    fileprivate let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red:0.13, green:0.44, blue:0.56, alpha:1.0)
        label.font = UIFont(name: "Avenir Medium", size: 20.0)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
       
        contentView.addSubview(title)
        
        
        
        title.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        title.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        
       
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

