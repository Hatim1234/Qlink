//
//  ViewControllerDashboard.swift
//  
//
//  Created by Hatim Belhadjhamida on 2019-07-13.
//

import UIKit
import Alamofire
import SwiftyJSON


class ViewControllerDashboard: UIViewController{
    
    var server = StudentServerAPI()
    
    var icsURL = readStringData(key: "icsUrl")
    
    @IBOutlet weak var courseName: UILabel!
    
    @IBOutlet weak var courseDates: UILabel!
    
    @IBOutlet weak var nextCourseDate: UILabel!
    
    @IBOutlet weak var courseImage: UIImageView!
    
    @IBOutlet weak var nextCourseName: UILabel!
    
    @IBOutlet weak var Location: UILabel!
    
    
    @IBOutlet weak var courseInfo: UIView!
    
    @IBOutlet weak var buildingImage: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
        
    @IBOutlet weak var courseLocation: UILabel!
    
    @IBOutlet weak var pager: UIPageControl!
    
    var dataTest = JSON()
    
    
    
    var blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffect.Style.dark))
    
    var data: [Course] = []
    
    @IBOutlet weak var upcomingCourse: UIView!
    
    
    @IBOutlet weak var todaysDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE, LLLL dd, yyyy"
        let todaysDate = dateFormatter.string(from: date)
        self.todaysDate.text = todaysDate
        
        getNext(){ courses in
            
            if !courses.isEmpty{
                
                var course = closestDate(courses: courses)
                
                
            
            
                self.buildingImage.image = course.backgroundImage
                self.nextCourseName.text = course.courseName
                
                let twendateFormatter = DateFormatter()
                let tweldateFormatter = DateFormatter()
                twendateFormatter.dateFormat = "HHmmss"
                tweldateFormatter.dateFormat = "h:mm a"

                let start = course.startDate.components(separatedBy: "T")[1]
                let end = course.endDate.components(separatedBy: "T")[1]



                let startTime = tweldateFormatter.string( from: twendateFormatter.date(from: start)!)

                let endTime = tweldateFormatter.string( from: twendateFormatter.date(from:end)!)

                self.nextCourseDate.text = startTime + "-" + endTime
                
                self.Location.text = course.getLocation()
                
            }
            
            else{
                self.buildingImage.isHidden = true
            }
            
        }
        
        server.GetWeeksCourses(icsUrl: self.icsURL){ dataStruct in
            self.data = dataStruct
            print(dataStruct)
            self.collectionView.reloadData()
            var check = self.data.count%3
            var checked = self.data.count/3
            if check == 0{
                self.pager.numberOfPages = checked
            }
            else{
                self.pager.numberOfPages = checked + 1
            }
        }
        
        
       
        
        
        self.courseDates.numberOfLines = 0 // 0
        
        self.blurEffectView.frame = view.bounds
        self.blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        
        self.courseInfo.addGestureRecognizer(tap)
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(CustomCell.self, forCellWithReuseIdentifier: "cell")
        
        self.buildingImage.roundCorners(corners: [.topLeft, .bottomLeft], radius: 10.0)
        self.courseImage.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
        self.courseInfo.layer.cornerRadius = 10
        self.upcomingCourse.layer.cornerRadius = 10
        
        
        
        
        
        
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
        }
    

    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.courseInfo.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
            self.courseInfo.alpha = 0
            self.blurEffectView.alpha = 0
            
           
        }) { (success:Bool) in
            self.courseInfo.removeFromSuperview()
            self.blurEffectView.removeFromSuperview()
        }
    }
    
    func getNext(completionBlock:@escaping ([Course]) -> ()){
                    
                let formatter = DateFormatter()
                formatter.dateFormat = "yyyy-MM-dd"
                formatter.timeZone = TimeZone(abbreviation: "EST")
                let estDate = formatter.string(from: Date()).components(separatedBy: "-")
                
                
        server.GetTodaysCourses(day: estDate[2], month: estDate[1], year: estDate[0], icsUrl: self.icsURL){ dataStruct in
                    
                    completionBlock(dataStruct)
                }
                   
               
    }
    
    

        
        
        
    }
        

    
    
    
   





extension ViewControllerDashboard: UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let witdh = scrollView.frame.width - (scrollView.contentInset.left*2)
        let index = scrollView.contentOffset.x / witdh
        let roundedIndex = round(index)
        self.pager?.currentPage = Int(roundedIndex)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/3.5, height: collectionView.frame.width/3.5)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CustomCell
        cell.bg.image = nil;
        cell.data = self.data[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        var course = self.data[indexPath.item]
        
        self.courseName.text = course.courseName
        
        var daysText = ""
        for ele in course.getDays(){
            
            daysText = daysText + ele.key + "    " + ele.value + "\n"
        }
        
        
        self.courseDates.text = daysText
        
        self.courseLocation.text = course.getLocation()
        
        var Height = self.courseDates.optimalHeight
        
        self.courseDates.frame = CGRect(x: self.courseDates.frame.origin.x, y: self.courseDates.frame.origin.y, width: self.courseDates.frame.width, height: Height)
        
        self.view.addSubview(self.blurEffectView)
        self.view.addSubview(self.courseInfo)
        self.courseInfo.center = self.view.center
        
        self.courseImage.image = course.backgroundImage
        
        self.courseInfo.transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
        self.courseInfo.alpha = 0

        UIView.animate(withDuration: 0.4) {
            self.blurEffectView.alpha = 1
            self.courseInfo.alpha = 1
            self.courseInfo.transform = CGAffineTransform.identity
        }
        
    }
}


class CustomCell: UICollectionViewCell {
    
    var data: Course? {
        didSet {
            guard let data = data else { return }
            bg.image = data.backgroundImage
            title.text = data.courseName
            
        }
    }
    
    fileprivate let bg: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.layer.cornerRadius = 12
        return iv
    }()
    
    fileprivate let title: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.white
        label.font = UIFont(name: "Avenir Light", size: 16.0)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        

        
        
        
        contentView.addSubview(bg)
        contentView.addSubview(title)
        
        title.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        title.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        
        bg.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        bg.leftAnchor.constraint(equalTo: contentView.leftAnchor).isActive = true
        bg.rightAnchor.constraint(equalTo: contentView.rightAnchor).isActive = true
        bg.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

