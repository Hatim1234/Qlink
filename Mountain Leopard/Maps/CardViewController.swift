//
//  CardViewController.swift
//  CardViewAnimation
//
//  Created by Hatim Belhadjhamida on 2020-01-03.
//  Copyright © 2020 Hatim Belhadjhamida. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation


class CardViewController: UIViewController {
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    
    
//    var datas: [Study] = [Study(buildingName: "Stauffer Library", closeOpen: false, hours: "9:00 AM - 10:00 PM"), Study(buildingName: "Bracken", closeOpen: false, hours: "9:00 AM - 8:00 PM"), Study(buildingName: "Library Archives", closeOpen: true, hours: "8:00 AM - 6:00 PM"), Study(buildingName: "Douglas Library", closeOpen: true, hours: "9:00 AM - 10:00 PM"), Study(buildingName: "Law Library", closeOpen: false, hours: "9:00 AM - 10:00 PM")]
    
    var datas: [Study] = loadStudy()
    
    var data: [Course] = [Course(courseName: "CISC 221", Location: "DUNNING HALL", startDate: "20190909T130000", endDate: "20190909T143000", days: [:]), Course(courseName: "CISC 221", Location: "DUNNING HALL", startDate: "20190909T130000", endDate: "20190909T143000", days: [:]),Course(courseName: "CISC 221", Location: "DUNNING HALL", startDate: "20190909T130000", endDate: "20190909T143000", days: [:]),Course(courseName: "CISC 221", Location: "DUNNING HALL", startDate: "20190909T130000", endDate: "20190909T143000", days: [:]),Course(courseName: "CISC 221", Location: "DUNNING HALL", startDate: "20190909T130000", endDate: "20190909T143000", days: [:])]

    @IBOutlet weak var handleArea: UIView!
    
    @IBOutlet weak var SlideCollectionView: UICollectionView!
    
   enum Option {
       case Study
       case Classes
       case Food
   }
    
    var choice = Option.Classes
    
    
    override func viewDidLoad() {
        self.view.layer.cornerRadius = 16
        
        switch choice {
        case .Classes:
            let nib = UINib(nibName: "CourseViewCell", bundle: Bundle(for: CourseViewCell.self))
            SlideCollectionView.register(nib, forCellWithReuseIdentifier: "CourseViewCell")
            
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
            formatter.timeZone = TimeZone(abbreviation: "EST")
            let estDate = formatter.string(from: Date()).components(separatedBy: "-")
            
            var icsURL = readStringData(key: "icsUrl")
            
            loadData(day: estDate[2], month: estDate[1], year: estDate[0], icsUrl: icsURL){ dataStruct in
                
                self.data = dataStruct
                self.SlideCollectionView.reloadData()
            }
        case .Study:
            print("Hello")
            let nib = UINib(nibName: "StudyCell", bundle: Bundle(for: StudyCell.self))
            SlideCollectionView.register(nib, forCellWithReuseIdentifier: "StudyCell")
            
        case .Food:
            print("In progress")
        }
        
        
        
        let nib = UINib(nibName: "CourseViewCell", bundle: Bundle(for: CourseViewCell.self))
        SlideCollectionView.register(nib, forCellWithReuseIdentifier: "CourseViewCell")
        
        flowLayout.sectionInset = UIEdgeInsets(top: 2, left: 2, bottom: 2, right: 2)
        
        flowLayout.minimumInteritemSpacing = 30
        flowLayout.minimumLineSpacing = 30
        
        SlideCollectionView.backgroundColor = .clear
        SlideCollectionView.delegate = self
        SlideCollectionView.dataSource = self
        
       
        
    }
    
    
    
    
}

extension CardViewController:
UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UICollectionViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           let witdh = scrollView.frame.width - (scrollView.contentInset.left*2)
           let index = scrollView.contentOffset.x / witdh
           let roundedIndex = round(index)
       }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
          
    if choice == .Study{
        
        return CGSize(width: 128, height: 102)
    }
             
    return CGSize(width: collectionView.frame.width/3.125, height: collectionView.frame.width/3.125)
          
          
      }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch choice {
        case .Classes:
             
            return data.count
        case .Study:
             
            return datas.count
            
        case .Food:
            print("In progress")
                    }
           
            return data.count
    }
    

   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
        switch choice {
        case .Classes:
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CourseViewCell", for: indexPath) as! CourseViewCell
                   cell.data = data[indexPath.item]
            cell.layer.backgroundColor = UIColor.white.cgColor
                   cell.layer.cornerRadius = 12
                  
                   cell.contentView.layer.cornerRadius = 12
                   cell.contentView.layer.borderWidth = 1.0
                  
                   cell.contentView.layer.borderColor = UIColor.clear.cgColor
                   cell.contentView.layer.masksToBounds = true
                  
                   cell.layer.shadowColor = UIColor.gray.cgColor
                   cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                   cell.layer.shadowRadius = 2.0
                   cell.layer.shadowOpacity = 1.0
                   cell.layer.masksToBounds = false
                   cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
             return cell
        case .Study:
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StudyCell", for: indexPath) as! StudyCell
                   cell.data = datas[indexPath.item]
            cell.layer.backgroundColor = UIColor.white.cgColor
                   cell.layer.cornerRadius = 12
                  
                   cell.contentView.layer.cornerRadius = 12
                   cell.contentView.layer.borderWidth = 1.0
                  
                   cell.contentView.layer.borderColor = UIColor.clear.cgColor
                   cell.contentView.layer.masksToBounds = true
                  
                   cell.layer.shadowColor = UIColor.gray.cgColor
                   cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
                   cell.layer.shadowRadius = 2.0
                   cell.layer.shadowOpacity = 1.0
                   cell.layer.masksToBounds = false
                   cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
             return cell
            
        case .Food:
            print("In progress")
                    }
           
        
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StudyCell", for: indexPath) as! StudyCell
                         cell.data = datas[indexPath.item]

            return cell
           
            
        }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        switch choice {
               case .Classes:
                    var course = self.data[indexPath.item]
                    var location = course.Location
                
                    ControllerInstance.zoominMarker(markerName: location)
                        
               case .Study:
                   var study = self.datas[indexPath.item]
                   var location = study.Location

                   ControllerInstance.zoominMarker(markerName: location)
               case .Food:
                   print("In progress")
                }
        
    }



    

    


}
    
    
    
    
    
    
    

