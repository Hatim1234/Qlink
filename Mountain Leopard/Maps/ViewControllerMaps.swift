//
//  ViewControllerMaps.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2019-07-13.
//  Copyright © 2019 Hatim Belhadjhamida. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Nominatim
var ControllerInstance = ViewControllerMaps()
class ViewControllerMaps: UIViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!
    
    @IBOutlet weak var map: GMSMapView!
    var markers: [GMSMarker] = []
    
  @IBOutlet weak var studyB: UIButton!
  
    @IBOutlet weak var classesB: UIButton!
    @IBOutlet weak var foodB: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ControllerInstance = self
        map.delegate = self
        map.animate(to: GMSCameraPosition.camera(withLatitude: 44.226821, longitude: -76.495626, zoom: 14.0))
        
        setupCard()
        
       
        
        
        
        
        
        

        // Do any additional setup after loading the view.
    }
    
    
    
    
    
    @IBAction func studyButton(_ sender: Any) {
        self.studyB.titleLabel?.font = self.studyB.titleLabel?.font.withSize(24)
        self.classesB.titleLabel?.font = self.classesB.titleLabel?.font.withSize(19)
        self.foodB.titleLabel?.font = self.foodB.titleLabel?.font.withSize(19)
        cardViewController.choice = .Study
        let nib = UINib(nibName: "StudyCell", bundle: Bundle(for: StudyCell.self))
        cardViewController.SlideCollectionView.register(nib, forCellWithReuseIdentifier: "StudyCell")
        cardViewController.SlideCollectionView.reloadData()
        placeMarkers(object: cardViewController.datas)
        
    }
    
   
    @IBAction func classButton(_ sender: Any) {
        self.classesB.titleLabel?.font = self.classesB.titleLabel?.font.withSize(24)
        self.foodB.titleLabel?.font = self.foodB.titleLabel?.font.withSize(19)
        self.studyB.titleLabel?.font = self.studyB.titleLabel?.font.withSize(19)
         cardViewController.choice = .Classes
        cardViewController.SlideCollectionView.reloadData()
        placeMarkers(object: cardViewController.data)
        
    }
    

    @IBAction func foodButton(_ sender: Any) {
        self.foodB.titleLabel?.font = self.foodB.titleLabel?.font.withSize(24)
        self.studyB.titleLabel?.font = self.studyB.titleLabel?.font.withSize(19)
        self.classesB.titleLabel?.font = self.classesB.titleLabel?.font.withSize(19)
        cardViewController.choice = .Food
        cardViewController.SlideCollectionView.reloadData()
        
    }
    
    func placeMarkers<T: student>(object: [T]) -> Void {
        map.clear()
        self.markers = []
        for ele in object{
            
            
            getCoordinate(addressString: (ele.Location).components(separatedBy: " ")[0] + " Hall, Kingston, ON, Canada"){ location, error in
                
                let position = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
                let marker = GMSMarker(position: position)
                marker.title = ele.Location
                marker.map = self.map
                self.markers.append(marker)
            }
        }
        
        
    }
    
    func zoominMarker(markerName: String) -> Void {
           
           var marker = getMarker(markerName: markerName)
           
           if marker != nil{
            map.animate(to:GMSCameraPosition.camera(withLatitude: marker!.position.latitude, longitude:  marker!.position.longitude, zoom: 20.0))
            
           }
       }
    
    func getMarker(markerName: String) ->GMSMarker? {
        for marker in self.markers{
            
            if marker.title! == markerName{
                return marker
                
            }
        }
        return nil
    }
    
    
    func getCoordinate( addressString : String,
            completionHandler: @escaping(CLLocationCoordinate2D, NSError?) -> Void ) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(addressString) { (placemarks, error) in
            if error == nil {
                if let placemark = placemarks?[0] {
                    let location = placemark.location!
                        
                    completionHandler(location.coordinate, nil)
                    return
                }
            }
                
            completionHandler(kCLLocationCoordinate2DInvalid, error as NSError?)
        }
    }
    
    
    
    
    //Fked up shit that involves the pull up card
    
    enum CardState {
        case expanded
        case collapsed
    }
    
    var cardViewController:CardViewController!
    var visualEffectView:UIVisualEffectView!
    
    let cardHeight:CGFloat = 600
    let cardHandleAreaHeight:CGFloat = 65
    
    var cardVisible = false
    var nextState:CardState {
        return cardVisible ? .collapsed : .expanded
    }
    
    var runningAnimations = [UIViewPropertyAnimator]()
    var animationProgressWhenInterrupted:CGFloat = 0
    
    
    func setupCard() {
        visualEffectView = UIVisualEffectView()
        //visualEffectView.frame = self.view.frame
        self.view.addSubview(visualEffectView)
        
        cardViewController = CardViewController(nibName:"CardViewController", bundle:nil)
        self.addChild(cardViewController)
        self.view.addSubview(cardViewController.view)
        cardViewController.view.frame = CGRect(x: 0, y:   self.cardHandleAreaHeight-(self.view.bounds.height*0.6342), width: self.view.bounds.width, height: cardHeight)
        
        cardViewController.view.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewControllerMaps().handleCardTap(recognzier:)))
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(ViewControllerMaps().handleCardPan(recognizer:)))
        
        cardViewController.handleArea.addGestureRecognizer(tapGestureRecognizer)
        cardViewController.handleArea.addGestureRecognizer(panGestureRecognizer)
        
        self.view.addSubview(studyB)
        self.view.addSubview(classesB)
        self.view.addSubview(foodB)
        
        
    }

    @objc
    func handleCardTap(recognzier:UITapGestureRecognizer) {
        switch recognzier.state {
        case .ended:
            animateTransitionIfNeeded(state: nextState, duration: 0.9)
        default:
            break
        }
    }
    
    @objc
    func handleCardPan (recognizer:UIPanGestureRecognizer) {
        switch recognizer.state {
        case .began:
            startInteractiveTransition(state: nextState, duration: 0.9)
        case .changed:
            let translation = recognizer.translation(in: self.cardViewController.handleArea)
            var fractionComplete = -translation.y / cardHeight
            fractionComplete = cardVisible ? fractionComplete : -fractionComplete
            updateInteractiveTransition(fractionCompleted: fractionComplete)
        case .ended:
            continueInteractiveTransition()
        default:
            break
        }
        
    }
    
    func animateTransitionIfNeeded (state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            let frameAnimator = UIViewPropertyAnimator(duration: duration, dampingRatio: 1) {
                switch state {
                case .expanded:
                    self.cardViewController.view.frame.origin.y = self.cardHandleAreaHeight-(self.view.bounds.height/2.2)
                case .collapsed:
                    self.cardViewController.view.frame.origin.y = self.cardHandleAreaHeight-(self.view.bounds.height*0.68)
                }
            }
            
            frameAnimator.addCompletion { _ in
                self.cardVisible = !self.cardVisible
                self.runningAnimations.removeAll()
            }
            
            frameAnimator.startAnimation()
            runningAnimations.append(frameAnimator)
            
            
            let cornerRadiusAnimator = UIViewPropertyAnimator(duration: duration, curve: .linear) {
                switch state {
                case .expanded:
                    self.cardViewController.view.layer.cornerRadius =  16
                case .collapsed:
                    self.cardViewController.view.layer.cornerRadius = 16
                }
            }
            
            cornerRadiusAnimator.startAnimation()
            runningAnimations.append(cornerRadiusAnimator)
            
           
            
            
            
        }
    }
    
    func startInteractiveTransition(state:CardState, duration:TimeInterval) {
        if runningAnimations.isEmpty {
            animateTransitionIfNeeded(state: state, duration: duration)
        }
        for animator in runningAnimations {
            animator.pauseAnimation()
            animationProgressWhenInterrupted = animator.fractionComplete
        }
    }
    
    func updateInteractiveTransition(fractionCompleted:CGFloat) {
        for animator in runningAnimations {
            animator.fractionComplete = fractionCompleted + animationProgressWhenInterrupted
        }
    }
    
    func continueInteractiveTransition (){
        for animator in runningAnimations {
            animator.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        }
    }

}
