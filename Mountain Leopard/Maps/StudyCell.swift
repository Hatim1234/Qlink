//
//  StudyCell.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2020-01-03.
//  Copyright © 2020 Hatim Belhadjhamida. All rights reserved.
//

import UIKit

class StudyCell: UICollectionViewCell {
    
    
    @IBOutlet weak var name: UILabel!
    
    @IBOutlet weak var open: UIImageView!
    
    @IBOutlet weak var times: UILabel!
    
    var data: Study? {
               didSet {
                   guard let data = data else { return }
                   
                self.name.text = data.Location
                if data.closeOpen == false{
                    self.open.backgroundColor = .red
                }
                
                self.times.text = data.hours
                   
                   
                   
               }
               
              
           }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
