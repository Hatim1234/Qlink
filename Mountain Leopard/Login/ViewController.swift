//
//  ViewController.swift
//  Mountain Leopard
//
//  Created by Hatim Belhadjhamida on 2019-07-12.
//  Copyright © 2019 Hatim Belhadjhamida. All rights reserved.
//

import UIKit
import WebKit
import Alamofire



class ViewController: UIViewController, WKNavigationDelegate{
    
    var server = StudentServerAPI()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        button.layer.cornerRadius = 22
        button.clipsToBounds = true
        
        userName.underlined()
        passWord.underlined()
        
        self.loading.isHidden = true
        
        self.hideKeyboard()
        
        userName.text = "17hb22"
        passWord.text = "HelloWorld1213!!"
        }
    
    
    
    @IBOutlet weak var button: UIButton!
    

    
    @IBOutlet var userName: UITextField!
    
    
    @IBOutlet var passWord: UITextField!
    
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    @IBOutlet weak var wrong: UILabel!
    
    var icsUrl: String = ""
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        writeAnyData(key: "icsUrl", value: self.icsUrl)
        writeAnyData(key: "username", value: self.userName.text!)
        writeAnyData(key: "password", value: self.passWord.text!)
        writeAnyData(key: "LoggedIN", value: true)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "tabbar")
        UIApplication.shared.keyWindow?.rootViewController = vc
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
    
    
    @IBAction func Login(_ sender: Any, forEvent event: UIEvent) {
        self.loading.startAnimating()
        self.wrong.isHidden = true
         self.loading.isHidden = false
        self.server.Login(usernameText: self.userName.text!, passwordText: self.passWord.text!){ response in
            if response["Result"] == "Login Failed"{
                self.loading.isHidden = true
                self.wrong.isHidden = false
                
            }
            if response["Result"] == "Success"{
                self.icsUrl = response["URL"]!
                self.performSegue(withIdentifier: "toDashboard", sender: self)
            }
            
        }
        
        
        
        
    }
    
   


    

}






